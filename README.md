# ClientWebdev

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.1.2.

## Procedimento para rodar o projeto frontend
- Rodar o comando "npm install" na pasta do projeto para instalar todas as dependências utilizadas;
- Rodar "ng serve" para rodar o servidor de desenvolvimento que estará funcionando na url padrão do Angular (localhost:4200);

OBS: Se após esses passos der algum erro para carregar os componentes do Angular Material, rode o comando "ng add @angular/material", escolhendo o tema Indigo/Pink e dando yes para as demais opções. 




