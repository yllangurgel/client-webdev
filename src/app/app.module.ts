import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './components/template/header/header.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { WorkspaceNavComponent } from './components/template/workspace-nav/workspace-nav.component';
import { MatListModule } from '@angular/material/list';
import { MatCardModule } from '@angular/material/card';
import { HomeComponent } from './views/home/home.component';
import { WorkspaceReadComponent } from './components/workspaces/workspace-read/workspace-read.component';
import { WorkspaceUpdateComponent } from './components/workspaces/workspace-update/workspace-update.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { ResourceCreateComponent } from './components/resources/resource-create/resource-create.component';
import { TabCreateComponent } from './components/tabs/tab-create/tab-create.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { WorkspaceCreateComponent } from './components/workspaces/workspace-create/workspace-create.component';

import { AuthInterceptor } from './interceptors/auth.interceptor';
import { AuthService } from './services/auth.service';
import { LoginComponent } from './views/login/login.component';
import { LoginCardComponent } from './views/login-card/login-card.component';
import { SignUpComponent } from './views/sign-up/sign-up.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    WorkspaceNavComponent,
    HomeComponent,
    WorkspaceReadComponent,
    WorkspaceUpdateComponent,
    ResourceCreateComponent,
    TabCreateComponent,
    LoginComponent,
    LoginCardComponent,
    SignUpComponent,
    WorkspaceCreateComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatMenuModule,
    MatIconModule,
    MatSidenavModule,
    MatListModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    FormsModule,
    HttpClientModule,
    MatSnackBarModule
  ],
  providers: [
    AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
