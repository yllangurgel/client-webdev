import { Component, ViewChild, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @ViewChild('loginForm') loginForm: HTMLFormElement;
  user = { username: "", password: "" };
  error = '';

  constructor(
    private authService: AuthService,
    private router: Router
  ) { 
    this.handleLoginError.bind(this);
  }

  ngOnInit(): void {
  }

  onSubmit() {
    const { username, password } = this.user;
    this.authService.login(username, password)
      .pipe(
        catchError(this.handleLoginError)
      )
      .subscribe((authInfo) => {
        this.router.navigate(['/']);
      });
  }

  handleLoginError = (err: any, caught: Observable<any>) => {
    if (err.status === 400 || err.status === 401)
    {
      this.error = 'Nome de usuário ou senha estão incorretos';
    } else {
      this.error = 'Aconteceu um erro ao enviar o formulário';
    }
    this.loginForm.controls['username'].setErrors({ 'incorrect': true })
    this.loginForm.controls['password'].setErrors({ 'incorrect': true })
    return of();
  }
}
