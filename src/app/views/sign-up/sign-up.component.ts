import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { AuthInfo, AuthService, SignUpData } from '../../services/auth.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  userInfo : SignUpData = {
    userName: '',
    email: '',
    password: '',
    passwordConfirmation: ''
  }
  error = '';

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  onSignUp() {
    this.authService.signUp(this.userInfo)
      .pipe(
        catchError(this.handleSignupError)
      )
      .subscribe(() => {
        this.router.navigate(['/']);
      })
  }

  handleSignupError = (err: any, caught: Observable<AuthInfo>) => {
    if (err.status === 400 || err.status === 401) 
    {
      this.error = 'Aconteceu um erro ao enviar seus dados, verifique se os campos estão corretos.'
    } else {
      this.error = 'Aconteceu um erro. Por favor, tente novamente mais tarde.'
    }
    return of();
  }
}
