import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Tab } from 'src/app/models/Tab';
import { TabService } from '../tab.service';

@Component({
  selector: 'app-tab-create',
  templateUrl: './tab-create.component.html',
  styleUrls: ['./tab-create.component.css']
})
export class TabCreateComponent implements OnInit {

  tab: Tab = {
    link: '',
    resourceId: +this.route.snapshot.params.resourceId
  }

  workspaceId: number;

  constructor(private router: Router, private route: ActivatedRoute, private tabService: TabService) { }

  ngOnInit(): void {
    this.workspaceId = +this.route.snapshot.params.workspaceId;
  }

  create() {
    this.tabService.post(this.workspaceId, this.tab).subscribe(() => {
      this.tabService.showMessage("Nova tab adicionada com sucesso!");
      this.router.navigate(['resources/id']);
    })
  }

  cancel() {
    this.router.navigate([`workspaces/${this.workspaceId}/resources/${this.tab.resourceId}`]);
  }

}
