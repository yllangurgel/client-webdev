import { Component, OnInit } from '@angular/core';
import { Resource } from 'src/app/models/Resource';
import { Workspace } from 'src/app/models/Workspace';
import { ResourcesService } from '../../resources/resources.service';
import { WorkspacesService } from '../../workspaces/workspaces.service';

@Component({
  selector: 'app-workspace-nav',
  templateUrl: './workspace-nav.component.html',
  styleUrls: ['./workspace-nav.component.css']
})
export class WorkspaceNavComponent implements OnInit {

  constructor(private workspaceService: WorkspacesService, private resourceService: ResourcesService) { }

  workspaces: Workspace[] = [];
  workspacesToList: any[] = [];

  ngOnInit(): void {
    this.workspaceService.getAll().subscribe(workspaces => {
      this.workspaces = workspaces;
      this.agruparResourcesWorkspaces();
    });
  }

  agruparResourcesWorkspaces() {
    this.workspaces.forEach(ws => {
      this.resourceService.getAllByWorkspaceId(ws.workspaceId).subscribe((resources) => {
        this.workspacesToList.push({ workspace: ws, resources });
      });
    });
  }

}
