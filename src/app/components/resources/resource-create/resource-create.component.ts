import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Workspace } from 'src/app/models/Workspace';
import { WorkspacesService } from '../../workspaces/workspaces.service';

@Component({
  selector: 'app-resource-create',
  templateUrl: './resource-create.component.html',
  styleUrls: ['./resource-create.component.css']
})
export class ResourceCreateComponent implements OnInit {

  workspace: Workspace = {
    name: ''
  }

  constructor(private router: Router, private workspaceService: WorkspacesService) { }

  ngOnInit(): void {
  }

  create() {
    this.workspaceService.post(this.workspace).subscribe(() => {
      this.workspaceService.showMessage("Workspace foi criado com sucesso!");
      this.router.navigate(['/']);
    })
  }

  cancel() {
    this.router.navigate(['/']);
  }
}
