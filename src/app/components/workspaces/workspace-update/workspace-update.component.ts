import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Resource } from 'src/app/models/Resource';
import { ResourcesService } from '../../resources/resources.service';

@Component({
  selector: 'app-workspace-update',
  templateUrl: './workspace-update.component.html',
  styleUrls: ['./workspace-update.component.css']
})
export class WorkspaceUpdateComponent implements OnInit {

  resource: Resource = {
    name: '',
    workspaceId: 0,
    resourceId: 0
  };
  workspaceId: number;
  resourceId: number;

  constructor(private router: Router, private route: ActivatedRoute, private resourceService: ResourcesService) { }

  ngOnInit(): void {
    this.workspaceId = +this.route.snapshot.params.workspaceId;
    this.resourceId = +this.route.snapshot.params.resourceId;
    this.resourceService.getById(this.workspaceId, this.resourceId).subscribe(resource => {
      this.resource = resource;
    });
  }

  update() {
    this.resourceService.put(this.resource.resourceId, this.workspaceId, this.resource.name).subscribe(() => {
      this.resourceService.showMessage("Resource alterado com sucesso!");
      this.router.navigate([`/workspaces/${this.workspaceId}/resources/${this.resourceId}`]);
    })
  }

  cancel() {
    this.router.navigate([`/workspaces/${this.workspaceId}/resources/${this.resourceId}`]);
  }

}
