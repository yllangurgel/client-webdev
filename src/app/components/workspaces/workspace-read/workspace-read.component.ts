import { HttpClientModule } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {Resource} from 'src/app/models/Resource';
import {ResourcesService} from '../../resources/resources.service';
import { TabService } from '../../tabs/tab.service';

@Component({
  selector: 'app-workspace-read',
  templateUrl: './workspace-read.component.html',
  styleUrls: ['./workspace-read.component.css']
})
export class WorkspaceReadComponent implements OnInit {

  workspaceId: number;
  resourceId: number;
  
  workspaceTabs = [];
  resourceTitle: string;

  constructor(
    private route: ActivatedRoute, 
    private tabService: TabService, 
    private resourceService: ResourcesService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.workspaceTabs = [];
      this.resourceId = this.route.snapshot.params.resourceId;
      this.workspaceId = this.route.snapshot.params.workspaceId;
      this.resourceService.getById(this.workspaceId, this.resourceId)
        .subscribe((resource) => {
          this.resourceTitle = resource.name; 
        });
      this.getAllTabs();
    });
  }

  getAllTabs() {
    this.tabService.getAll(this.workspaceId, this.resourceId).subscribe((tabs) => {
      this.workspaceTabs = tabs;
    });
  }

  excluir(id: number) {
    const confirmation = confirm("Deseja mesmo excluir esta aba?");
    if(confirmation) {
      this.tabService.delete(this.workspaceId, this.resourceId, id).subscribe(() => {
        this.tabService.showMessage("Aba excluída com sucesso!");
        this.router.navigate([`workspaces/${this.workspaceId}/resources/${this.resourceId}`]);
      });
    }

  }

  openWorkspaceTabs() {
    // usando a api de tabs para fechar as tabs existentes e abrir as referentes ao workspace
  }

}
