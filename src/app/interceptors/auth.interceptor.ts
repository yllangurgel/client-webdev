import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpHeaders,
} from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, exhaustMap } from 'rxjs/operators';

import { AuthService, TokenInfo } from '../services/auth.service';
import { JwtToken } from '../models/jwt-token.model';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private authService: AuthService,
  ) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const jsonToken = localStorage.getItem(AuthService.JWT_TOKEN_INFO);
    // Não está autenticado, continua o request normal
    // Se estiver obtendo a refresh-token, não é necessário a token de acesso.
    if (jsonToken == null || request.url.includes('refresh-token'))
    {
      return next.handle(request);
    }

    const tokenInfo : TokenInfo = JSON.parse(jsonToken);
    const token = JwtToken.from(tokenInfo);
    let tokenObs = token.isExpired ? this.authService.refreshToken() : of(token);

    return tokenObs
      .pipe(
        map((token: JwtToken) => 
          new HttpHeaders().set('Authorization', `Bearer ${token.accessToken}`)
        ),
        exhaustMap((headers: HttpHeaders) => {
          const modRequest = request.clone({ headers });
          return next.handle(modRequest);
        })
      );
  }
}
