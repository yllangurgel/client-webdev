export interface Tab {
    tabId?: number,
    link: string,
    resourceId: number
}