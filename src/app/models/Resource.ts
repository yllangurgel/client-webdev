export interface Resource {
    resourceId?: number,
    name: string,
    workspaceId: number
}